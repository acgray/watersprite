from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from judging.models import JudgeProfile
from django.contrib.humanize.templatetags.humanize import naturalday, naturaltime
from django.core.exceptions import ValidationError
from django.utils import timezone


class EmailOrUsernameModelBackend(object):
    def authenticate(self, username=None, password=None):
        if '@' in username:
            kwargs = {'email': username}
        else:
            kwargs = {'username': username}
        try:
            user = User.objects.get(**kwargs)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

class TimeRestrictedBackend(EmailOrUsernameModelBackend):
    def authenticate(self, username=None, password=None):
        user = super(TimeRestrictedBackend,self).authenticate(username, password)

        try:
            # Get user judging group if they have one
            jp = JudgeProfile.objects.get(user=user)
            if jp.judge_group and jp.judge_group.open_date and jp.judge_group.open_date > timezone.now():
                open_date = jp.judge_group.open_date
                datestr = "%s at %s" % (naturalday(open_date), open_date.strftime('%H:%M'))
                if naturalday(open_date) != u'today' and naturalday(open_date) != u'tomorrow':
                    datestr = 'on '+datestr
                raise ValidationError("Judging has not started yet. Please come back %s when you will be able to access the system." % datestr)
            elif jp.judge_group and jp.judge_group.close_date and jp.judge_group.close_date < timezone.now():
                raise ValidationError("Judging has now finished")
            else:
                return user

        except JudgeProfile.DoesNotExist:
            return user

