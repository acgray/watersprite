from __future__ import absolute_import
import os
from time import sleep
from celery import Celery
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE')
app = Celery('watersprite', broker='amqp://guest@localhost', backend='amqp')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

@app.task(bind=True)
def convert_task(self):
    print('Request: {0!r}'.format(self.request))

@app.task
def add(x,y):
    sleep(5)
    print x+y
    return x+y