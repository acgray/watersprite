from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from django.views.generic.base import TemplateView
from entries.views import EntryCreateView
from judging import views as judging

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'watersprite.views.home', name='home'),
    # url(r'^watersprite/', include('watersprite.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^$', EntryCreateView.as_view()),

    url(r'^judging/$', 'judging.views.index', name='judging_index'),
    url(r'^judging/(\d+)$', 'judging.views.entry_rating', name='entry_rating'),
    url(r'^judging/welcome/$', judging.WelcomeInterstitialView.as_view(), name="welcome_interstitial"),

    (r'^accounts/login/$', 'django.contrib.auth.views.login'),
    url(r'^accounts/logout$', 'django.contrib.auth.views.logout', kwargs={'next_page': '/judging'}, name='logout'),

    (r'^conversion_endpoint$', 'entries.views.conversion_endpoint_view')
)
