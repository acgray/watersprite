from fabric.api import *
from fabric.state import env
from paramiko import RSAKey
import os

# env.hosts = ['10.39.9.52']
# env.user = "ubuntu"
# env.key_filename = ['/home/ubuntu/.ssh/watersprite.pem']
#
def convert_file(filename):
    command = """ffmpeg -nostats -i %s -vcodec libx264 -vprofile main -preset fast -b:v 500k -maxrate 500k -bufsize 1000k -vf scale=-1:480 -s 640x480 -acodec libfaac -b:a 128k -pix_fmt yuv420p -threads 1 converted/%s.mp4"""
    #command = """ffmpeg -nostats -i %s -vcodec libx264 -vprofile main -preset veryfast -vf scale=-1:480 -s 640x480 -an -threads 0 -acodec libfaac -pix_fmt yuv420p -threads 0 converted/%s.mp4"""
    run('s3cmd get --skip-existing --no-progress s3://watersprite-uploads/film_files/%s' % filename)
    run(command % (filename, filename))
    run('s3cmd put --no-progress converted/%s.mp4 s3://watersprite-uploads/converted/' % filename)
    run('rm %s converted/%s.mp4' % (filename, filename))

def test():
    run('uname -a')

env.hosts = ['entries2.watersprite.org.uk']
env.user = "ubuntu"
env.project_dir = "~/watersprite/"

def manage_py(command):
    with cd(env.project_dir):
        run('./manage.py %s' % command)

def collectstatic():
    manage_py('collectstatic --noinput')

def migrate():
    manage_py('migrate --all')

def git_pull():
    with cd(env.project_dir):
        run('git pull')

def install_requirements():
    with cd(env.project_dir):
        run('pip install -r requirements.txt')

def restart():
    with cd(env.project_dir):
        run('sudo service watersprite restart')

def deploy():
    git_pull()
    install_requirements()
    migrate()
    #collectstatic()
    restart()

