import datetime

from django.core.mail.message import EmailMessage
from django.db import models
from django.db.models import ForeignKey
from django.db.models.aggregates import Avg
from django.template.loader import render_to_string
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _
from queued_storage.backends import QueuedStorage


LANGUAGE_CHOICES = (
    (u'1','is filmed in English'),
    (u'2','has English subtitles'),
)

ROLE_CHOICES = (
    ('director','Director'),
    ('producer','Producer'),
    ('crew','Crew'),
    ('other','Other')
)

GENRE_CHOICES = (
    ('an','Animation'),
    ('do','Documentary'),
    ('fi','Fiction'),
)

SUBMISSION_METHOD_CHOICES = (
    ('dvd','DVD'),
    ('upload','Upload'),
)

COUNTRY_CHOICES = (
    ('GB', _('United Kingdom')),
    ('AF', _('Afghanistan')),
    ('AX', _('Aland Islands')),
    ('AL', _('Albania')),
    ('DZ', _('Algeria')),
    ('AS', _('American Samoa')),
    ('AD', _('Andorra')),
    ('AO', _('Angola')),
    ('AI', _('Anguilla')),
    ('AQ', _('Antarctica')),
    ('AG', _('Antigua and Barbuda')),
    ('AR', _('Argentina')),
    ('AM', _('Armenia')),
    ('AW', _('Aruba')),
    ('AU', _('Australia')),
    ('AT', _('Austria')),
    ('AZ', _('Azerbaijan')),
    ('BS', _('Bahamas')),
    ('BH', _('Bahrain')),
    ('BD', _('Bangladesh')),
    ('BB', _('Barbados')),
    ('BY', _('Belarus')),
    ('BE', _('Belgium')),
    ('BZ', _('Belize')),
    ('BJ', _('Benin')),
    ('BM', _('Bermuda')),
    ('BT', _('Bhutan')),
    ('BO', _('Bolivia')),
    ('BA', _('Bosnia and Herzegovina')),
    ('BW', _('Botswana')),
    ('BV', _('Bouvet Island')),
    ('BR', _('Brazil')),
    ('IO', _('British Indian Ocean Territory')),
    ('BN', _('Brunei Darussalam')),
    ('BG', _('Bulgaria')),
    ('BF', _('Burkina Faso')),
    ('BI', _('Burundi')),
    ('KH', _('Cambodia')),
    ('CM', _('Cameroon')),
    ('CA', _('Canada')),
    ('CV', _('Cape Verde')),
    ('KY', _('Cayman Islands')),
    ('CF', _('Central African Republic')),
    ('TD', _('Chad')),
    ('CL', _('Chile')),
    ('CN', _('China')),
    ('CX', _('Christmas Island')),
    ('CC', _('Cocos (Keeling) Islands')),
    ('CO', _('Colombia')),
    ('KM', _('Comoros')),
    ('CG', _('Congo')),
    ('CD', _('Congo, The Democratic Republic of the')),
    ('CK', _('Cook Islands')),
    ('CR', _('Costa Rica')),
    ('CI', _('Cote d\'Ivoire')),
    ('HR', _('Croatia')),
    ('CU', _('Cuba')),
    ('CY', _('Cyprus')),
    ('CZ', _('Czech Republic')),
    ('DK', _('Denmark')),
    ('DJ', _('Djibouti')),
    ('DM', _('Dominica')),
    ('DO', _('Dominican Republic')),
    ('EC', _('Ecuador')),
    ('EG', _('Egypt')),
    ('SV', _('El Salvador')),
    ('GQ', _('Equatorial Guinea')),
    ('ER', _('Eritrea')),
    ('EE', _('Estonia')),
    ('ET', _('Ethiopia')),
    ('FK', _('Falkland Islands (Malvinas)')),
    ('FO', _('Faroe Islands')),
    ('FJ', _('Fiji')),
    ('FI', _('Finland')),
    ('FR', _('France')),
    ('GF', _('French Guiana')),
    ('PF', _('French Polynesia')),
    ('TF', _('French Southern Territories')),
    ('GA', _('Gabon')),
    ('GM', _('Gambia')),
    ('GE', _('Georgia')),
    ('DE', _('Germany')),
    ('GH', _('Ghana')),
    ('GI', _('Gibraltar')),
    ('GR', _('Greece')),
    ('GL', _('Greenland')),
    ('GD', _('Grenada')),
    ('GP', _('Guadeloupe')),
    ('GU', _('Guam')),
    ('GT', _('Guatemala')),
    ('GG', _('Guernsey')),
    ('GN', _('Guinea')),
    ('GW', _('Guinea-Bissau')),
    ('GY', _('Guyana')),
    ('HT', _('Haiti')),
    ('HM', _('Heard Island and McDonald Islands')),
    ('VA', _('Holy See (Vatican City State)')),
    ('HN', _('Honduras')),
    ('HK', _('Hong Kong')),
    ('HU', _('Hungary')),
    ('IS', _('Iceland')),
    ('IN', _('India')),
    ('ID', _('Indonesia')),
    ('IR', _('Iran, Islamic Republic of')),
    ('IQ', _('Iraq')),
    ('IE', _('Ireland')),
    ('IM', _('Isle of Man')),
    ('IL', _('Israel')),
    ('IT', _('Italy')),
    ('JM', _('Jamaica')),
    ('JP', _('Japan')),
    ('JE', _('Jersey')),
    ('JO', _('Jordan')),
    ('KZ', _('Kazakhstan')),
    ('KE', _('Kenya')),
    ('KI', _('Kiribati')),
    ('KP', _('Korea, Democratic People\'s Republic of')),
    ('KR', _('Korea, Republic of')),
    ('KW', _('Kuwait')),
    ('KG', _('Kyrgyzstan')),
    ('LA', _('Lao People\'s Democratic Republic')),
    ('LV', _('Latvia')),
    ('LB', _('Lebanon')),
    ('LS', _('Lesotho')),
    ('LR', _('Liberia')),
    ('LY', _('Libyan Arab Jamahiriya')),
    ('LI', _('Liechtenstein')),
    ('LT', _('Lithuania')),
    ('LU', _('Luxembourg')),
    ('MO', _('Macao')),
    ('MK', _('Macedonia, The Former Yugoslav Republic of')),
    ('MG', _('Madagascar')),
    ('MW', _('Malawi')),
    ('MY', _('Malaysia')),
    ('MV', _('Maldives')),
    ('ML', _('Mali')),
    ('MT', _('Malta')),
    ('MH', _('Marshall Islands')),
    ('MQ', _('Martinique')),
    ('MR', _('Mauritania')),
    ('MU', _('Mauritius')),
    ('YT', _('Mayotte')),
    ('MX', _('Mexico')),
    ('FM', _('Micronesia, Federated States of')),
    ('MD', _('Moldova')),
    ('MC', _('Monaco')),
    ('MN', _('Mongolia')),
    ('ME', _('Montenegro')),
    ('MS', _('Montserrat')),
    ('MA', _('Morocco')),
    ('MZ', _('Mozambique')),
    ('MM', _('Myanmar')),
    ('NA', _('Namibia')),
    ('NR', _('Nauru')),
    ('NP', _('Nepal')),
    ('NL', _('Netherlands')),
    ('AN', _('Netherlands Antilles')),
    ('NC', _('New Caledonia')),
    ('NZ', _('New Zealand')),
    ('NI', _('Nicaragua')),
    ('NE', _('Niger')),
    ('NG', _('Nigeria')),
    ('NU', _('Niue')),
    ('NF', _('Norfolk Island')),
    ('MP', _('Northern Mariana Islands')),
    ('NO', _('Norway')),
    ('OM', _('Oman')),
    ('PK', _('Pakistan')),
    ('PW', _('Palau')),
    ('PS', _('Palestinian Territory, Occupied')),
    ('PA', _('Panama')),
    ('PG', _('Papua New Guinea')),
    ('PY', _('Paraguay')),
    ('PE', _('Peru')),
    ('PH', _('Philippines')),
    ('PN', _('Pitcairn')),
    ('PL', _('Poland')),
    ('PT', _('Portugal')),
    ('PR', _('Puerto Rico')),
    ('QA', _('Qatar')),
    ('RE', _('Reunion')),
    ('RO', _('Romania')),
    ('RU', _('Russian Federation')),
    ('RW', _('Rwanda')),
    ('BL', _('Saint Barthelemy')),
    ('SH', _('Saint Helena')),
    ('KN', _('Saint Kitts and Nevis')),
    ('LC', _('Saint Lucia')),
    ('MF', _('Saint Martin')),
    ('PM', _('Saint Pierre and Miquelon')),
    ('VC', _('Saint Vincent and the Grenadines')),
    ('WS', _('Samoa')),
    ('SM', _('San Marino')),
    ('ST', _('Sao Tome and Principe')),
    ('SA', _('Saudi Arabia')),
    ('SN', _('Senegal')),
    ('RS', _('Serbia')),
    ('SC', _('Seychelles')),
    ('SL', _('Sierra Leone')),
    ('SG', _('Singapore')),
    ('SK', _('Slovakia')),
    ('SI', _('Slovenia')),
    ('SB', _('Solomon Islands')),
    ('SO', _('Somalia')),
    ('ZA', _('South Africa')),
    ('GS', _('South Georgia and the South Sandwich Islands')),
    ('ES', _('Spain')),
    ('LK', _('Sri Lanka')),
    ('SD', _('Sudan')),
    ('SR', _('Suriname')),
    ('SJ', _('Svalbard and Jan Mayen')),
    ('SZ', _('Swaziland')),
    ('SE', _('Sweden')),
    ('CH', _('Switzerland')),
    ('SY', _('Syrian Arab Republic')),
    ('TW', _('Taiwan, Province of China')),
    ('TJ', _('Tajikistan')),
    ('TZ', _('Tanzania, United Republic of')),
    ('TH', _('Thailand')),
    ('TL', _('Timor-Leste')),
    ('TG', _('Togo')),
    ('TK', _('Tokelau')),
    ('TO', _('Tonga')),
    ('TT', _('Trinidad and Tobago')),
    ('TN', _('Tunisia')),
    ('TR', _('Turkey')),
    ('TM', _('Turkmenistan')),
    ('TC', _('Turks and Caicos Islands')),
    ('TV', _('Tuvalu')),
    ('UG', _('Uganda')),
    ('UA', _('Ukraine')),
    ('AE', _('United Arab Emirates')),
    ('US', _('United States')),
    ('UM', _('United States Minor Outlying Islands')),
    ('UY', _('Uruguay')),
    ('UZ', _('Uzbekistan')),
    ('VU', _('Vanuatu')),
    ('VE', _('Venezuela')),
    ('VN', _('Viet Nam')),
    ('VG', _('Virgin Islands, British')),
    ('VI', _('Virgin Islands, U.S.')),
    ('WF', _('Wallis and Futuna')),
    ('EH', _('Western Sahara')),
    ('YE', _('Yemen')),
    ('ZM', _('Zambia')),
    ('ZW', _('Zimbabwe'))
    )

queued_s3storage = QueuedStorage(
    'django.core.files.storage.FileSystemStorage',
    'storages.backends.s3boto.S3BotoStorage',
    task='entries.tasks.TransferAndConvert')

# Create your models here.
class Entry(models.Model):
    film_name = models.CharField(max_length=255)

    first_name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    email_address = models.EmailField(max_length=255,help_text="Please enter the main contact email address here. This will be used for all correspondence unless you subsequently change it with us by email.")
    phone_number = models.CharField(max_length=255,help_text="including country code if outside the UK")
    address = models.TextField(help_text='Please use the address where you would like any awards to be posted to if you win but are unable to attend the ceremony')
    country = models.CharField(max_length=2,choices=COUNTRY_CHOICES)
    university = models.CharField(max_length=255,verbose_name='University or College',help_text="Please note - it is part of the terms and conditions that you were a student at the time of making this film. We may ask you for proof of matriculation in the case of any dispute!")
    main_contact_role = models.CharField(choices=ROLE_CHOICES, max_length=10, verbose_name="Your Role", null=True)

    film_synopsis = models.TextField(help_text="Please include a synopsis for your film, in English, here. Suggested word count: 100-150 words.")
    film_length = models.CharField(max_length=255,help_text="E.g. 14:34. The maximum film length is 20 minutes. Any films longer than this will automatically be disqualified from the competition - so please enter the correct value here!")
    film_language = models.CharField(max_length=1, choices=LANGUAGE_CHOICES, help_text="Any films received that are filmed in a foreign language and do not have English subtitles will have to be disqualified. This is because we do not have the language capacity to make sure all films can be judged fairly if in foreign languages.", verbose_name='this film',default=1)
    original_language = models.CharField(max_length=255,blank=True)
    genre = models.CharField(max_length=3,choices=GENRE_CHOICES, null=True)
    award_categories = models.ManyToManyField('judging.RatingCriterion', max_length=2)
    directors_name = models.CharField(max_length=255, verbose_name="Director's name")
    directors_email = models.EmailField(verbose_name="Director's email",null=True)
    directors_address = models.TextField(verbose_name="Director's street address",null=True)
    producers_name = models.CharField(max_length=255, verbose_name="Producer's name")
    producers_email = models.EmailField(verbose_name="Producer's email",null=True)
    producers_address = models.TextField(verbose_name="Producer's street address",null=True)
    screenshot = models.ImageField(upload_to='screenshots',help_text='Please upload up to 3 screenshots you would like to be associated with your film in any publicity. Accepted formats are jpg, gif or png files.',storage=queued_s3storage)
    screenshot2 = models.ImageField(upload_to='screenshots', blank=True, null=True)
    screenshot3 = models.ImageField(upload_to='screenshots', blank=True, null=True)
    press_pack = models.FileField(upload_to='press_packs', blank=True, null=True, help_text="If you have an existing press pack for your film you may upload it here.")
    previous_awards = models.TextField(help_text="If your film has won any awards at other festivals, please list them here.", blank=True, null=True)

    tc_agree = models.BooleanField()
    judging_portal_agree = models.BooleanField(default=True)
    submission_method = models.CharField(max_length=6,choices=SUBMISSION_METHOD_CHOICES,default='upload',verbose_name="I am submitting my film to Watersprite via")

    film_file = models.FileField(blank=True,null=True,upload_to='film_files',help_text='Maximum file size 700mb. Allowed formats: .3g2, .3gp, .asf, .asx, .avi, .bdm, .flv, .mov, .mp4, .mpeg, .mpg, .swf, .wmv', storage=queued_s3storage)
    film_file_converted = models.FileField(blank=True,null=True,upload_to='converted')

    uploaded_on = models.DateTimeField(auto_now_add=True)

    # Non-public fields
    competition = models.ForeignKey('Competition', null=True)
    is_waiting_list = models.BooleanField(default=False)

    def delete(self, *args, **kwargs):
        for f in [self.film_file,self.screenshot]:
            if f:
                f.delete()
        super(Entry,self).delete(*args,**kwargs)

    def save(self, **kwargs):
        if not self.competition_id:
            self.competition = Competition.objects.latest()
        return super(Entry,self).save(**kwargs)

    @property
    def is_dvd(self):
        return self.submission_method == 'dvd'

    def is_complete(self):
        return self.film_file.name is not u''
    is_complete.boolean = True

    def is_converted(self):
        try:
            if len(self.film_file_converted.name) > 0:
                return True
            else:
                return False
        except (AttributeError,TypeError):
            return False

    is_converted.boolean = True

    class Meta:
        verbose_name_plural = 'entries'

    def __unicode__(self):
        return "%s (%s)" % (unicode(self.film_name),unicode(self.get_genre_display()))

    def send_confirmation(self):
        em = EmailMessage('Watersprite entry confirmation: #WS2013-%s' % self.id,
                          render_to_string('entries/success_email.txt', {'entry': self}),
                          'submissions@watersprite.org',
                          [self.email_address],
                          cc=['submissions@watersprite.org'])
        return em.send()

    def get_genre(self):
        # replacement for choice field
        return Genre.objects.get(shortcode=self.genre)

    def get_code(self):
        return "%s-%s" % (self.competition.code, self.id)

    def submit_for_conversion(self):
        from entries.utils import submit_for_conversion
        return submit_for_conversion([self])


class Genre(models.Model):
    name = models.CharField(max_length=255)
    shortcode = models.CharField(max_length=2,blank=True)
    def __unicode__(self):
        return unicode(self.name)

    def get_shortlist(self):
        try:
            current_comp = Competition.objects.latest()
        except Competition.DoesNotExist:
            return
        return Entry.objects.filter(competition=current_comp,genre=self.shortcode).annotate(avg=Avg('rating__overall')).order_by('-avg')[:20]

class Competition(models.Model):
    name = models.CharField(max_length=255, help_text='N.B. This will be displayed on the frontend')
    code = models.CharField(max_length=10, blank=True, null=True, help_text=lambda: "Prepended to entry ids and used for folder names.  Something like 'WS%s'" % (datetime.datetime.now().year+1), default=lambda: (datetime.datetime.now().year+1))
    entries_open = models.DateTimeField()
    entries_close = models.DateTimeField()
    entries_limit = models.IntegerField(help_text='Leave blank for no limit', blank=True, null=True)
    current_from = models.DateTimeField()

    def __unicode__(self):
        return self.name

    def is_full(self):
        return self.entry_set.count() >= self.entries_limit

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = slugify(self.name)

        return super(Competition, self).save(*args, **kwargs)

    class Meta:
        get_latest_by = 'current_from'

class ConversionJob(models.Model):
    entry = ForeignKey(to=Entry)
    job_id = models.CharField(max_length=255, primary_key=True)
    status = models.CharField(max_length=15, blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    message = models.TextField(blank=True,null=True)
