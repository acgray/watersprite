from queued_storage.tasks import TransferAndDelete


class TransferAndConvert(TransferAndDelete):

    def transfer(self, name, local, remote, **kwargs):
        result = super(TransferAndConvert, self).transfer(name, local, remote, **kwargs)

        from entries.models import Entry
        from entries.utils import submit_for_conversion

        if result:
            entry_qs = Entry.objects.filter(film_file=name)
            submit_for_conversion(entry_qs)

        return result