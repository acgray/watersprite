from django.core.management.base import BaseCommand
import os
from entries.models import Entry
from fabric.tasks import execute
from fabfile import convert_file
from entries import tasks

class Command(BaseCommand):
    help = 'Convert entries without converted file'

    def handle(self, *args, **options):
        qs = Entry.objects.all()
        for e in qs:
            if e.film_file.name and not e.film_file_converted.name:
                self.stdout.write("Submitting entry #%s for conversion\n" % e.id)
                tasks.convert_entry.delay(e)
