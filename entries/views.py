# Create your views here.
import json
import logging
import urllib2

from django.http import HttpResponse
from django.template.context import RequestContext
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import CreateView
from django.shortcuts import render_to_response

from entries.forms import EntryForm
from entries.models import Entry, Competition, ConversionJob


logger = logging.getLogger(__name__)

DEFAULT_CLOSE_DATE = timezone.datetime(2013, 11, 29, 23, 59, tzinfo=timezone.utc)
DEFAULT_OPEN_DATE = timezone.now()

class EntryCreateView(CreateView):
    model = Entry
    form_class = EntryForm

    def get_context_data(self, **kwargs):
        context = super(EntryCreateView,self).get_context_data(**kwargs)
        context['current_competition'] = Competition.objects.latest()
        return context

    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        logger.debug('Sent JSON data: '+str(data))
        response_kwargs['content_type'] = 'application/json'
        response = HttpResponse(data, **response_kwargs)
        response['Access-Control-Allow-Origin'] = "*"
        return response

    def form_invalid(self, form):
        logger.debug('Received data: '+str(dict(form.data)))
        logger.debug('Errors: '+str(form.errors))
        if self.request.is_ajax():
            err_list = [{'field': i,'errors':form.errors[i]} for i in form.errors]
            return self.render_to_json_response(err_list, status=400)
        else:
            return super(EntryCreateView, self).form_invalid(form)

    def form_valid(self, form):
        self.object = form.save()
        logger.debug('Saved entry with id %s' % self.object.id)
        # Set waiting list flag if current competition has reached limit
        if Competition.objects.latest().is_full():
            self.object.is_waiting_list = True
            self.object.save()
        self.object.send_confirmation()
        if self.request.is_ajax():
            return render_to_response('entries/success_common.html',{'object':self.object},context_instance=RequestContext(self.request))
        else:
            return render_to_response('entries/success.html',{'object':self.object},context_instance=RequestContext(self.request))

    def post(self, request, *args, **kwargs):
        logger.debug('Received data: '+str(request.POST))
        return super(EntryCreateView,self).post(request,*args,**kwargs)

    def get(self, request, *args, **kwargs):
        if not request.user.is_staff:
            try:
                current_comp = Competition.objects.latest()
                close_date = current_comp.entries_close
                open_date = current_comp.entries_open
            except Competition.DoesNotExist:
                close_date = DEFAULT_CLOSE_DATE
                open_date = DEFAULT_OPEN_DATE
                current_comp = None
            if timezone.now() > close_date or timezone.now() < open_date:
                context = {
                    'after_close': timezone.now() > close_date,
                    'before_open': timezone.now() < open_date,
                    'current_competition': current_comp
                }
                return render_to_response('entries/closed.html', context)
        return super(EntryCreateView,self).get(request,*args,**kwargs)


@csrf_exempt
def conversion_endpoint_view(request):
    try:
        data = json.loads(request.body)
    except ValueError:
        return HttpResponse('Could not parse data', status=400)

    print data

    ## automatically reply to subscribe requests
    if data.get('Type') == 'SubscriptionConfirmation':
        confirmation_url = data.get('SubscribeURL')
        response = urllib2.urlopen(confirmation_url)
        return HttpResponse('OK')

    if data.get('Type') == 'Notification':
        try:
            message = json.loads(data.get('Message'))
        except ValueError:
            return HttpResponse('Could not parse message', status=400)
        job_id=message.get('jobId')
        try:
            cj = ConversionJob.objects.get(job_id=job_id)
            cj.status = message.get('state')
            cj.message = message.get('messageDetails')
            cj.save()
            if message.get('state') == 'COMPLETED':
                cj.entry.film_file_converted.name = message['outputs'][0]['key']
                cj.entry.save()
        except (ConversionJob.DoesNotExist, KeyError):
            pass
        return HttpResponse('OK')




    return HttpResponse('OK')
