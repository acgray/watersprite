import random
import string
from boto import elastictranscoder
from django.conf import settings

from entries.models import ConversionJob


def submit_for_conversion(queryset):

    # Open an elastic transcoder connection
    conn = elastictranscoder.connect_to_region(
        settings.AWS_REGION,
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
    for entry in queryset:
        if entry.film_file_converted:
            continue
        output_key = 'converted/%s/%s-%s.mp4' %\
                     (entry.competition.code,
                      entry.get_code(),
                      ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(4)))
        args = {
            "pipeline_id": settings.AWS_ELASTIC_TRANSCODER_PIPELINE,
            'input_name': {
                'Key': entry.film_file.file.key.key,
            },
            'output': {
                'Key': output_key,
                'PresetId': '1351620000001-100070'
            },
        }
        job = conn.create_job(**args)
        print job

        try:
            job_id = job['Job']['Id']
            ConversionJob.objects.create(entry=entry, job_id=job_id)
        except KeyError:
            pass