

$(function() {


    $(document).ajaxSend(function(event, xhr, settings) {
        function getCookie(name) {
            var cookieValue = null;
            if (document.cookie && document.cookie != '') {
                var cookies = document.cookie.split(';');
                for (var i = 0; i < cookies.length; i++) {
                    var cookie = jQuery.trim(cookies[i]);
                    // Does this cookie string begin with the name we want?
                    if (cookie.substring(0, name.length + 1) == (name + '=')) {
                        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                        break;
                    }
                }
            }
            return cookieValue;
        }
        function sameOrigin(url) {
            // url could be relative or scheme relative or absolute
            var host = document.location.host; // host + port
            var protocol = document.location.protocol;
            var sr_origin = '//' + host;
            var origin = protocol + sr_origin;
            // Allow absolute or scheme relative URLs to same origin
            return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                // or any other URL that isn't scheme relative or absolute i.e relative.
                !(/^(\/\/|http:|https:).*/.test(url));
        }
        function safeMethod(method) {
            return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
        }

        if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    });

    var checkUploadForm = function() {
        if ($('#id_submission_method_1').attr('checked') == 'checked') {
            $('#film_file_field').show();
        } else {
            $('#film_file_field').hide('rollup');
        }
    };
    checkUploadForm();

    var updateUploadPc = function(pc) {
        $('#progress_inner').css({width: pc+"%"});
        $('#upload_pc').html(pc+'%');
        if (pc > 50) {
            $('#upload_pc').css({color:'#000'});
        }
        if (pc == 100) {
            $('#upload_pc').html('Processing, please wait...');
        }
    };

    $('.errorlist').parents('tr').addClass('error');

    $('#id_submission_method_1,#id_submission_method_0').change(function() {
        checkUploadForm();
    });

    $('#id_original_language').parents('tr').hide();
    $('#id_film_language').change(function() {
        if ($(this).val() == 2) {
            $('#id_original_language').parents('tr').show('fade');
        } else {
            $('#id_original_language').parents('tr').hide('fade');
        }
    });

    $('#entry_form').bind('submit', function() {
        $('#progress_bar').hide();
        $('#working_overlay').show();
        $('#upload_status').html('Your file is currently being uploaded. If you have a slow internet connection this may take a significant amount of time. Your browser may indicate the progress of your upload in the bottom corner.')
    });

//    if (false) {
//        $('#entry_form').ajaxForm({
//              crossDomain: false,
//              beforeSend: function() {
//                  $('#working_overlay').show();
//                  $('#entry_form ul.error_list').remove();
//                  $('#entry_form .error').removeClass('error');
//                  updateUploadPc(0);
//              },
//              uploadProgress: function(event,position,total,pc) {
//                  updateUploadPc(pc);
//              },
//              complete: function(xhr) {
//                  console.log(xhr.responseText);
//                  $('#working_overlay').hide();
//                  if (xhr.status == 400) {
//                      $('#entry_form ul.errorlist').remove();
//                      $('#entry_form .error').removeClass('error');
//                      var errs = $.parseJSON(xhr.responseText);
//                      $.each(errs, function(e,f) {
//                          var error_list=$('<ul></ul>').addClass('errorlist');
//                          $.each(f.errors,function(i,t) {
//                              error_list.append($('<li>'+t+'</li>'));
//                          });
//
//                          $('#id_'+ f.field).parents('tr').addClass('error').find('td').prepend(error_list);
//
//                      });
//                      var first_id = '#id_'+errs[0].field;
//                      $.scrollTo($(first_id).parents('fieldset'),1000);
//                      setTimeout(function() {$(first_id).parents('tr').effect('highlight', {}, 1500)}, 1200);
//                  }
//                  if (xhr.status == 200) {
//                      $('#application').html(xhr.responseText)
//                  }
//              }
//        });
//    }
});