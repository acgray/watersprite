# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding M2M table for field award_categories on 'Entry'
        db.create_table('entries_entry_award_categories', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('entry', models.ForeignKey(orm['entries.entry'], null=False)),
            ('ratingcriterion', models.ForeignKey(orm['judging.ratingcriterion'], null=False))
        ))
        db.create_unique('entries_entry_award_categories', ['entry_id', 'ratingcriterion_id'])


    def backwards(self, orm):
        # Removing M2M table for field award_categories on 'Entry'
        db.delete_table('entries_entry_award_categories')


    models = {
        'entries.entry': {
            'Meta': {'object_name': 'Entry'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'award_categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['judging.RatingCriterion']", 'max_length': '2', 'symmetrical': 'False'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'directors_address': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'directors_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'directors_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'email_address': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'film_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_file_converted': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_language': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'film_length': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_synopsis': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'judging_portal_agree': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'main_contact_role': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'original_language': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'press_pack': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'previous_awards': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'producers_address': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'producers_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'producers_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'screenshot': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'screenshot2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'screenshot3': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'submission_method': ('django.db.models.fields.CharField', [], {'default': "'upload'", 'max_length': '6'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tc_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'uploaded_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        'entries.genre': {
            'Meta': {'object_name': 'Genre'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'shortcode': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'})
        },
        'judging.ratingcriterion': {
            'Meta': {'object_name': 'RatingCriterion'},
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'criteria'", 'symmetrical': 'False', 'to': "orm['entries.Genre']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['entries']