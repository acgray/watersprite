# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ConversionJob.message'
        db.add_column(u'entries_conversionjob', 'message',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'ConversionJob.message'
        db.delete_column(u'entries_conversionjob', 'message')


    models = {
        u'entries.competition': {
            'Meta': {'object_name': 'Competition'},
            'code': ('django.db.models.fields.CharField', [], {'default': '2014', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'current_from': ('django.db.models.fields.DateTimeField', [], {}),
            'entries_close': ('django.db.models.fields.DateTimeField', [], {}),
            'entries_limit': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'entries_open': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'entries.conversionjob': {
            'Meta': {'object_name': 'ConversionJob'},
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Entry']"}),
            'job_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'primary_key': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '15', 'null': 'True', 'blank': 'True'}),
            'updated': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        u'entries.entry': {
            'Meta': {'object_name': 'Entry'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'award_categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['judging.RatingCriterion']", 'max_length': '2', 'symmetrical': 'False'}),
            'competition': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Competition']"}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'directors_address': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'directors_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'directors_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'email_address': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'film_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_file_converted': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_language': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'film_length': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_synopsis': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_waiting_list': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'judging_portal_agree': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'main_contact_role': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'original_language': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'press_pack': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'previous_awards': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'producers_address': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'producers_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'producers_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'screenshot': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'screenshot2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'screenshot3': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'submission_method': ('django.db.models.fields.CharField', [], {'default': "'upload'", 'max_length': '6'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tc_agree': ('django.db.models.fields.BooleanField', [], {}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'uploaded_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'entries.genre': {
            'Meta': {'object_name': 'Genre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'shortcode': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'})
        },
        u'judging.ratingcriterion': {
            'Meta': {'object_name': 'RatingCriterion'},
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'criteria'", 'symmetrical': 'False', 'to': u"orm['entries.Genre']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['entries']