# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Entry'
        db.create_table('entries_entry', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('film_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('surname', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('email_address', self.gf('django.db.models.fields.EmailField')(max_length=255)),
            ('phone_number', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('address', self.gf('django.db.models.fields.TextField')()),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=2)),
            ('university', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('film_synopsis', self.gf('django.db.models.fields.TextField')()),
            ('film_length', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('film_language', self.gf('django.db.models.fields.CharField')(default=1, max_length=1)),
            ('original_language', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('directors_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('producers_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('screenshot', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('tc_agree', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('submission_method', self.gf('django.db.models.fields.CharField')(default='upload', max_length=6)),
            ('film_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('uploaded_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('entries', ['Entry'])


    def backwards(self, orm):
        # Deleting model 'Entry'
        db.delete_table('entries_entry')


    models = {
        'entries.entry': {
            'Meta': {'object_name': 'Entry'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'directors_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'email_address': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'film_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_language': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'film_length': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_synopsis': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original_language': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'producers_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'screenshot': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'submission_method': ('django.db.models.fields.CharField', [], {'default': "'upload'", 'max_length': '6'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tc_agree': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'uploaded_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['entries']