import os

from django import forms
from django.core.exceptions import ValidationError

from entries.models import Entry


class EntryForm(forms.ModelForm):
    class Meta:
        model = Entry
        widgets = {
            'submission_method': forms.RadioSelect,
            'award_categories': forms.CheckboxSelectMultiple
        }
        exclude = ['competition', 'is_waiting_list']

    def clean_tc_agree(self):
        data = self.cleaned_data['tc_agree']
        if not data:
            raise ValidationError('You must agree to the Terms & Conditions')
        return data

    def clean_film_file(self):
        data = self.cleaned_data['film_file']
        if self.data[u'submission_method'] == u'upload' and not data:
            raise ValidationError('You have selected to submit your film via upload but have not uploaded a file')
        if data:
            filename = data.name
            ext = os.path.splitext(filename)[1]
            ext = ext[1:].lower()
            if str(ext.lower()) not in ['3g2','3gp','asf','asx','avi','bdm','flv','mov','mp4','mpeg','mpg','swf','wmv']:
                raise ValidationError('Your film file must be in one of the allowed formats')
            if data._size > 734003200:
                raise ValidationError('File is too large')
        return data

    def clean_submission_method(self):
        if self.data[u'submission_method'] != u'upload':
            raise ValidationError('The deadline for DVD entries has now passed so you must submit your entry by upload.')
        return self.data['submission_method']

    def clean_award_categories(self):
        data = self.cleaned_data['award_categories']
        if len(data) > 2:
            raise ValidationError('Please select a maximum of 2 award categories.')
        return data
