from django.contrib import admin
from django.core.mail.message import EmailMessage
from django.db.models import FieldDoesNotExist
from django.template.loader import render_to_string

from entries.models import Entry, Genre, Competition, ConversionJob
from entries.utils import submit_for_conversion
from judging.admin import RatingInline


class CSVAdmin(admin.ModelAdmin):
    """
    Adds a CSV export action to an admin view.
    """

    # This is the maximum number of records that will be written.
    # Exporting massive numbers of records should be done asynchronously.
    csv_record_limit = 1000

    extra_csv_fields = ()

    def get_actions(self, request):
        actions = self.actions if hasattr(self, 'actions') else []
        actions.append('csv_export')
        actions = super(CSVAdmin, self).get_actions(request)
        return actions

    def get_extra_csv_fields(self, request):
        return self.extra_csv_fields

    def csv_export(self, request, qs=None, *args, **kwargs):
        import csv
        from django.http import HttpResponse
        from django.template.defaultfilters import slugify

        response = HttpResponse(mimetype='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' \
            % slugify(self.model.__name__)
        headers = list(self.list_display) + list(self.get_extra_csv_fields(request))
        writer = csv.DictWriter(response, headers)

        # Write header.
        header_data = {}
        for name in headers:
            if hasattr(self, name) \
            and hasattr(getattr(self, name), 'short_description'):
                header_data[name] = getattr(
                    getattr(self, name), 'short_description')
            else:
                try:
                    field = self.model._meta.get_field_by_name(name)
                    if field and field[0].verbose_name:
                        header_data[name] = field[0].verbose_name
                    else:
                        header_data[name] = name
                except FieldDoesNotExist:
                    header_data[name] = name
            header_data[name] = header_data[name].title()
        writer.writerow(header_data)

        # Write records.
        for r in qs[:self.csv_record_limit]:
            data = {}
            for name in headers:
                if hasattr(r, name):
                    data[name] = getattr(r, name)
                elif hasattr(self, name):
                    data[name] = getattr(self, name)(r)
                else:
                    raise Exception, 'Unknown field: %s' % (name,)

                if callable(data[name]):
                    data[name] = data[name]()
            data = {k: unicode(v).encode('utf8') for k,v in data.items()}
            writer.writerow(data)
        return response
    csv_export.short_description = \
        'Exported selected %(verbose_name_plural)s as CSV'


class IsConvertedListFilter(admin.SimpleListFilter):
    title = 'converted'
    parameter_name = 'converted'

    def lookups(self, request, model_admin):
        return (
            (1, 'Yes'),
            (0, 'No')
        )

    def queryset(self, request, queryset):
        if self.value() == '1':
            return queryset.exclude(film_file_converted='')
        elif self.value() == '0':
            return queryset.filter(film_file_converted='')
        return queryset


class EntryAdmin(CSVAdmin):
    list_display = ('get_code','film_name','first_name','surname','email_address','genre','submission_method','university','country', 'is_complete', 'is_converted', 'uploaded_on','competition', 'is_waiting_list', 'ratings_count')
    actions = ['resend_confirmation','send_dvd_email','submit_for_conversion']
    search_fields = ['film_name','first_name','surname']
    list_filter = ['competition','genre','award_categories', IsConvertedListFilter, ]
    inlines = [RatingInline,]

    def ratings_count(self, obj):
        return obj.rating_set.count()

    def resend_confirmation(self, request, queryset):
        for obj in queryset:
            obj.send_confirmation()
        self.message_user(request, "%s entries' confirmation emails were resent" % queryset.count())
    resend_confirmation.short_description = 'Resend confirmation emails for selected entries'

    def send_dvd_email(self, request, queryset):
        for obj in queryset:
            em = EmailMessage('Watersprite DVD receipt: #WS2013-%s' % obj.id,
                              render_to_string('entries/dvd_email.txt', {'entry': obj}),
                              'submissions@watersprite.org',
                              [obj.email_address],
                              cc=['submissions@watersprite.org'])
            em.send()
        self.message_user(request, "%s emails were sent" % queryset.count())
    send_dvd_email.short_description = 'Send DVD receipt emails for selected entries'

    def submit_for_conversion(self, request, queryset):
        submit_for_conversion(queryset)
        self.message_user(request, "%s conversion jobs were created!" % queryset.count())

class ConversionJobAdmin(admin.ModelAdmin):
    model = ConversionJob
    list_display = ['job_id', 'entry', 'status', 'updated']
    readonly_fields = ['job_id', 'entry', 'status', 'updated', 'message']

admin.site.register(Entry,EntryAdmin)
admin.site.register(Genre)
admin.site.register(Competition)
admin.site.register(ConversionJob, ConversionJobAdmin)
