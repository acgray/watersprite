# Watersprite Entries Portal

Bespoke submission and viewing platform developed for the [Watersprite Film Festival](http://www.watersprite.org.uk).

## Main features

* Automatic video conversion via the Amazon Elastic Transcoder API
* Numeric voting aggregation as per Watersprite competition regulations

## Author

Adam Gray <info@adamgray.me.uk>

## License

THiS IS PROPRIETARY SOFTWARE.  THE SOURCE CODE IS PROVIDED FOR INFORMATION ONLY AND MAY DISAPPEAR AT ANY TIME.