# Create your views here.
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.template.context import RequestContext
from django.views.generic import TemplateView
from django.views.generic.edit import UpdateView, CreateView, BaseCreateView
from querystring_parser import parser
from entries.models import Entry, Genre
from judging.models import Rating, RatingCriterion, RatingScore, JudgeProfile

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator


class LoginRequiredMixin(object):
    u"""Ensures that user must be authenticated in order to access view."""

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)


@login_required
def index(request):
    u = request.user
    try:
        if u.get_profile().is_new:
            return redirect('welcome_interstitial')
        entries = u.get_profile().entry_package.entries.all().exclude(rating__judge=u)
    except AttributeError:
        entries = []
    except JudgeProfile.DoesNotExist:
        raise Http404

    context = RequestContext(request)
    context['entries'] = entries
    context['already_rated'] = Entry.objects.filter(rating__judge=u)
    return render_to_response('judging/index.html',context)

@login_required
def entry_rating(request,*args,**kwargs):
    package = request.user.get_profile().entry_package
    try:
        entry = package.entries.get(id=args[0])
    except Entry.DoesNotExist:
        raise Http404()

    if Rating.objects.filter(judge=request.user,entry=entry).count() > 0:
        messages.add_message(request, messages.ERROR, 'Your have already rated that entry')
        return redirect(reverse('judging_index'))

    if request.POST:
        post_dict = parser.parse(request.POST.urlencode())
        r = Rating.objects.create(entry=entry,judge=request.user,comment=request.POST.get('comment',''),overall=request.POST.get('overall',5))
        for x,y in post_dict['criteria'].items():
            c = RatingCriterion.objects.get(id=x)
            RatingScore.objects.create(rating=r,criterion=c,score=y)
        messages.add_message(request, messages.SUCCESS, 'Your rating for %s has been recorded' % entry.film_name)
        print post_dict
        if post_dict.has_key('submit_next'):
            try:
                next = package.entries.exclude(id=entry.id)[0]
                return redirect(reverse('entry_rating',args=(next.id,)))
            except IndexError:
                messages.add_message(request, messages.ERROR, 'You have no more entries to rate!')
                return redirect(reverse('judging_index'))
        else:
            return redirect(reverse('judging_index'))


    genre = Genre.objects.get(shortcode=entry.genre)
    context = RequestContext(request)
    context['entry'] = entry
    context['genre'] = genre
    context['suggest_criteria'] = RatingCriterion.objects.filter(wildcard_allowed=True).exclude(id__in=[rc.id for rc in entry.award_categories.all()])
    return render_to_response('judging/rating.html', context)

class EntryRatingView(LoginRequiredMixin, CreateView):

    template_name = 'judging/rating.html'
    model = Rating

    def get(self, request, *args, **kwargs):
        self.package = request.user.get_profile().entry_package
        try:
            self.entry = self.package.entries.get(id=kwargs.get('eid'))
        except Entry.DoesNotExist:
            raise Http404()
        return super(BaseCreateView,self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.package = request.user.get_profile().entry_package
        try:
            self.entry = self.package.entries.get(id=kwargs.get('eid'))
        except Entry.DoesNotExist:
            raise Http404()
        self.object = self.model(judge=request.user,entry=self.entry)
        return super(BaseCreateView,self).get(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        post_dict = parser.parse(self.request.POST.urlencode())
        for x,y in post_dict['criteria'].items():
            c = RatingCriterion.objects.get(id=x)
            RatingScore.objects.create(rating=self.object,criterion=c,score=y)
        messages.add_message(self.request, messages.SUCCESS, 'Your rating for %s has been recorded' % entry.film_name)
        return self.get_success_url()

    def get_success_url(self):
        if self.request.POST.has_key('submit_next'):
            try:
                next = self.package.entries.exclude(id=self.entry.id)[0]
                return reverse('entry_rating',next.id)
            except IndexError:
                messages.add_message(self.request, messages.ERROR, 'You have no more entries to rate!')
                return reverse('judging_index')
        else:
            return redirect(reverse('judging_index'))

    def get_context_data(self, **kwargs):
        context = super(EntryRatingView,self).get_context_data(**kwargs)
        context['entry'] = self.entry
        genre = Genre.objects.get(shortcode=self.entry.genre)
        context['genre'] = genre
        return context

class WelcomeInterstitialView(TemplateView):
    template_name = 'judging/welcome.html'

    def post(self, request, *args, **kwargs):
        if request.POST.get('continue'):
            try:
                jp = request.user.get_profile()
                jp.is_new = False
                jp.save()
            except JudgeProfile.DoesNotExist:
                pass
            return redirect('judging_index')
        else:
            return super(WelcomeInterstitialView,self).get(request, *args, **kwargs)