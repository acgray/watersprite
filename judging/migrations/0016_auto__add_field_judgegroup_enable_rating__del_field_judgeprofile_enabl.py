# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'JudgeGroup.enable_rating'
        db.add_column(u'judging_judgegroup', 'enable_rating',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)

        # Deleting field 'JudgeProfile.enable_rating'
        db.delete_column(u'judging_judgeprofile', 'enable_rating')


    def backwards(self, orm):
        # Deleting field 'JudgeGroup.enable_rating'
        db.delete_column(u'judging_judgegroup', 'enable_rating')

        # Adding field 'JudgeProfile.enable_rating'
        db.add_column(u'judging_judgeprofile', 'enable_rating',
                      self.gf('django.db.models.fields.BooleanField')(default=True),
                      keep_default=False)


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'entries.competition': {
            'Meta': {'object_name': 'Competition'},
            'code': ('django.db.models.fields.CharField', [], {'default': '2014', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'current_from': ('django.db.models.fields.DateTimeField', [], {}),
            'entries_close': ('django.db.models.fields.DateTimeField', [], {}),
            'entries_limit': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'entries_open': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'entries.entry': {
            'Meta': {'object_name': 'Entry'},
            'address': ('django.db.models.fields.TextField', [], {}),
            'award_categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['judging.RatingCriterion']", 'max_length': '2', 'symmetrical': 'False'}),
            'competition': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Competition']"}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'directors_address': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'directors_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'directors_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'email_address': ('django.db.models.fields.EmailField', [], {'max_length': '255'}),
            'film_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_file_converted': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'film_language': ('django.db.models.fields.CharField', [], {'default': '1', 'max_length': '1'}),
            'film_length': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'film_synopsis': ('django.db.models.fields.TextField', [], {}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'genre': ('django.db.models.fields.CharField', [], {'max_length': '3', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_waiting_list': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'judging_portal_agree': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'main_contact_role': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True'}),
            'original_language': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'press_pack': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'previous_awards': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'producers_address': ('django.db.models.fields.TextField', [], {'null': 'True'}),
            'producers_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True'}),
            'producers_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'screenshot': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'screenshot2': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'screenshot3': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'submission_method': ('django.db.models.fields.CharField', [], {'default': "'upload'", 'max_length': '6'}),
            'surname': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tc_agree': ('django.db.models.fields.BooleanField', [], {}),
            'university': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'uploaded_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'entries.genre': {
            'Meta': {'object_name': 'Genre'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'shortcode': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'})
        },
        u'judging.entrypackage': {
            'Meta': {'object_name': 'EntryPackage'},
            'entries': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['entries.Entry']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'judges': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'entry_packages'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        },
        u'judging.judgegroup': {
            'Meta': {'object_name': 'JudgeGroup'},
            'close_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'enable_rating': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'open_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'judging.judgeprofile': {
            'Meta': {'object_name': 'JudgeProfile'},
            'entry_package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['judging.EntryPackage']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_new': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'judge_group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['judging.JudgeGroup']", 'null': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'judging.rating': {
            'Meta': {'object_name': 'Rating'},
            'comment': ('django.db.models.fields.TextField', [], {}),
            'created_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'entry': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['entries.Entry']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'judge': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'overall': ('django.db.models.fields.IntegerField', [], {})
        },
        u'judging.ratingcriterion': {
            'Meta': {'object_name': 'RatingCriterion'},
            'genres': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'criteria'", 'symmetrical': 'False', 'to': u"orm['entries.Genre']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wildcard_allowed': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        },
        u'judging.ratingscore': {
            'Meta': {'object_name': 'RatingScore'},
            'criterion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['judging.RatingCriterion']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'rating': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['judging.Rating']"}),
            'score': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['judging']