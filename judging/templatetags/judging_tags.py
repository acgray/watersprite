from django import template

register = template.Library()

@register.tag
def user_has_rated_entry(parser,token):
    bits = token.split_contents()[1:]
    if len(bits) != 1:
        raise template.TemplateSyntaxError('%r tag takes exactly one argument')
    return True