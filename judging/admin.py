from django.conf.urls import patterns
from django.contrib import admin
from django.contrib.auth.models import User
from django.core.mail.message import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.template.loader import render_to_string
from entries.models import Genre
from judging.models import EntryPackage, JudgeProfile, Rating, RatingScore, RatingCriterion, JudgeGroup


class EntryPackageAdmin(admin.ModelAdmin):
    list_display = ('id','label','entry_count','entry_list_text')
    filter_horizontal = ('entries','judges',)
    actions = ['download_screenshots']
    def download_screenshots(self,request,queryset):
        import os,tempfile,urllib,zipfile,io
        d = tempfile.mkdtemp()
        for ep in queryset:
            epd = os.path.join(d,"%s - %s" % (ep.id,ep.label))
            os.mkdir(epd)
            for e in ep.entries.all():
                url = e.screenshot.storage.url(e.screenshot.file.name)
                filename = url.split('/')[-1].split('#')[0].split('?')[0]
                fn,ext = os.path.splitext(filename)
                target = os.path.join(epd,'%s - %s%s' % (e.id,e.film_name,ext))
                urllib.urlretrieve(url,target)
        buffer = io.BytesIO()
        zip = zipfile.ZipFile(buffer,"w", zipfile.ZIP_DEFLATED)
        rootlen = len(d) + 1
        for base,dirs,files in os.walk(d):
            for file in files:
                fn = os.path.join(base,file)
                zip.write(fn,fn[rootlen:])
        zip.close()
        buffer.flush()

        ret_zip = buffer.getvalue()
        buffer.close()

        response = HttpResponse(ret_zip, mimetype='application/zip')
        response['Content-Disposition'] = 'attachment; filename=archive.zip'

        return response


class RatingScoreInline(admin.StackedInline):
    model = RatingScore
    extra = 0
    fields = ('criterion', 'score','is_wildcard',)
    readonly_fields = ('is_wildcard',)

class RatingAdmin(admin.ModelAdmin):
    change_list_template = 'judging/admin/rating/change_list.html'
    model = Rating
    list_display = ('id','entry','judge','overall','created_date','comment')
    inlines = [RatingScoreInline,]
    search_fields = ['entry__film_name','judge__email','judge__first_name','judge__last_name','comment']
    list_filter = ['entry__competition']

    def shortlist_view(self,request):
        context = RequestContext(request)
        include_wildcard = request.GET.get('include_wildcard', '1') == '1'

        context['criteria'] = [{'criterion': c, 'shortlist': c.get_shortlist(include_wildcard=include_wildcard)}
                               for c in RatingCriterion.objects.all()]
        context['genres'] = [{'genre': g, 'shortlist': g.get_shortlist()} for g in Genre.objects.all()]
        context['include_wildcard'] = include_wildcard

        return render_to_response('judging/admin/shortlists.html',context)

    def get_urls(self):
        urls = super(RatingAdmin,self).get_urls()
        my_urls = patterns('',
            (r'^shortlists/$', self.shortlist_view)
        )
        return my_urls+urls

class JudgeProfileAdmin(admin.ModelAdmin):
    model = JudgeProfile
    list_display = ('user','entry_package','enable_rating','judge_group','has_finished_rating')
    search_fields = ['user__email']
    readonly_fields = ('user',)
    actions = ['reset_password','enable_rating','disable_rating', 'mark_as_new', 'mark_as_not_new']
    def reset_password(self,request,queryset):
        for jp in queryset:
            pw = User.objects.make_random_password()
            u = jp.user
            u.set_password(pw)
            u.save()
            message = EmailMessage('Watersprite online judging',render_to_string('judging/welcome_email.txt',{'user':u,'password':pw}),'webmaster@watersprite.org',[u.email])
            message.send()
            self.message_user(request, "%s emails were sent" % queryset.count())
    reset_password.short_description = "Reset password & send welcome email"

    def mark_as_new(self,request,queryset):
        queryset.update(is_new=True)
        self.message_user(request, "%s judges were marked as new" % queryset.count())

    def mark_as_not_new(self,request,queryset):
        queryset.update(is_new=False)
        self.message_user(request, "%s judges were marked as not new" % queryset.count())

    # def disable_rating(self,request,queryset):
    #     queryset.update(enable_rating=False)
    #     self.message_user(request, "Disabled rating for %s users" % queryset.count())
    #
    # def enable_rating(self,request,queryset):
    #     queryset.update(enable_rating=True)
    #     self.message_user(request, "Enabled rating for %s users" % queryset.count())


class RatingInline(admin.StackedInline):
    model = Rating
    extra = 0

class RatingCriterionAdmin(admin.ModelAdmin):
    list_display = ('name', 'wildcard_allowed',)

admin.site.register(EntryPackage,EntryPackageAdmin)
admin.site.register(JudgeProfile,JudgeProfileAdmin)
admin.site.register(JudgeGroup)
admin.site.register(Rating,RatingAdmin)
admin.site.register(RatingCriterion,RatingCriterionAdmin)
