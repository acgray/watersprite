from django.contrib.auth.models import User
from django.db import models
from django.db.models import F
from django.db.models.aggregates import Avg
from django.db.models.signals import post_save
from entries.models import Entry, Genre, Competition


class EntryPackage(models.Model):
    entries = models.ManyToManyField(to=Entry,blank=True)
    judges = models.ManyToManyField(to=User,related_name='entry_packages')
    label = models.CharField(max_length=255, blank=True, help_text="Warning: this label is visible to the judging users")
    def entry_count(self):
        return self.entries.count()
    def entry_list_text(self):
        return '\n'.join([entry.film_name for entry in self.entries.all()])
    def __unicode__(self):
        if self.label:
            return '%s (#%s)' % (self.label, self.id)
        return 'Entry package #%s' % self.id

def create_user_profile(sender, instance, created, **kwargs):
    if created:
        JudgeProfile.objects.create(user=instance)
post_save.connect(create_user_profile, sender=User)

class RatingCriterion(models.Model):
    name = models.CharField(max_length=255)
    genres = models.ManyToManyField(to=Genre,related_name='criteria')
    wildcard_allowed = models.BooleanField(default=True)

    def __unicode__(self):
        return unicode(self.name)

    def get_shortlist(self, include_wildcard=True):
        try:
            current_comp = Competition.objects.latest()
        except Competition.DoesNotExist:
            return

        scores = self.ratingscore_set.filter(rating__entry__competition=current_comp)
        if not include_wildcard:
            scores = scores.filter(wildcard=False)

        shortlist = scores.values('rating__entry').annotate(avg=Avg('score')).order_by('-avg')[:20]
        return [{'entry': Entry.objects.get(id=r['rating__entry']), 'avg': r['avg'], 'wildcard_count': scores.filter(wildcard=True, rating__entry=r['rating__entry']).count()} for r in shortlist]

    class Meta:
        verbose_name_plural = 'rating criteria'

class RatingScore(models.Model):
    criterion = models.ForeignKey(to=RatingCriterion)
    score = models.IntegerField()
    rating = models.ForeignKey(to='Rating')
    wildcard = models.BooleanField(default=False)
    def __unicode__(self):
        return "%s: %s (%s)" % (self.criterion.name, self.score, self.rating.entry.film_name)

    @property
    def is_wildcard(self):
        return self.criterion not in self.rating.entry.award_categories.all()

    def save(self, *args, **kwargs):
        self.wildcard = self.is_wildcard
        return super(RatingScore, self).save(*args, **kwargs)

class Rating(models.Model):
    entry = models.ForeignKey(to=Entry)
    judge = models.ForeignKey(to=User)
    comment = models.TextField()
    overall = models.IntegerField()
    created_date = models.DateTimeField(auto_now_add=True)


class JudgeProfile(models.Model):
    user = models.OneToOneField(to=User,blank=True,null=True)
    entry_package = models.ForeignKey(to=EntryPackage,blank=True,null=True)
    judge_group = models.ForeignKey(to='JudgeGroup', blank=True, null=True)
    is_new = models.BooleanField(default=True)

    @property
    def enable_rating(self):
        if self.judge_group:
            return self.judge_group.enable_rating
        return False

    def __unicode__(self):
        return unicode(self.user.email)

    def has_rated(self, entry):
        return len(Rating.objects.filter(judge=self.user, entry=entry)) > 0

    def has_finished_rating(self):
        if self.entry_package:
            return Rating.objects.filter(judge=self.user, entry__in=self.entry_package.entries.all()).count() == self.entry_package.entries.count()
        return False

class JudgeGroup(models.Model):
    name = models.CharField(max_length=255)
    open_date = models.DateTimeField(blank=True, null=True, help_text="Users will not be able to access the system before this time")
    close_date = models.DateTimeField(blank=True, null=True, help_text="Users will not be able to access the system after this time")
    enable_rating = models.BooleanField(default=True)
    def __unicode__(self):
        return self.name
