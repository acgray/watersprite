import csv
from django.contrib.auth.models import User, Group
from django.core import mail
from django.core.mail.message import EmailMessage
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string
from entries.models import Competition
from judging.models import EntryPackage

class Command(BaseCommand):
    help = 'Import judges from given CSV file'

    def handle(self, *args, **options):
        connection = mail.get_connection()
        filename = args[0]
        judges_group = Group.objects.get(name='Judges')
        current_competition = Competition.objects.latest()
        with open(filename, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                row = [v.decode('utf8') for v in row]
                info = {
                    'first_name': row[0],
                    'last_name': row[1],
                    'email': row[2],
                    'package_id': row[3],
                }
                self.stdout.write(u'Importing judge %s %s (%s) with package %s ' % (row[0],row[1],row[2],row[3]))
                password = User.objects.make_random_password(length=8)

                u,created = User.objects.get_or_create(username=row[2],defaults={
                    'email' :row[2],
                    'first_name': row[0],
                    'last_name': row[1],
                    })
                if created:
                    u.set_password(password)
                    u.groups.add(judges_group)
                    u.save()

                profile = u.get_profile()
                try:
                    profile.entry_package = EntryPackage.objects.get(id=int(row[3]))
                except EntryPackage.DoesNotExist:
                    self.stdout.write('FAILED: Entry package number %s does not exist!!' % row[3])
                profile.judge_group_id = row[4]
                profile.save()

                if created:
                    message = EmailMessage('Watersprite Online Judging',render_to_string('judging/welcome_email.txt',{'user':u,'password':password, 'competition': current_competition}),'webmaster@watersprite.org',[u.email],connection=connection)
                else:
                    message = EmailMessage('Watersprite account updated', render_to_string('judging/update_email.txt',{'user':u}), 'webmaster@watersprite.org',[u.email],connection=connection)
                message.send()

                if created:
                    self.stdout.write('Created user with id %s\n' % u.id)
                else:
                    self.stdout.write('Updated existing user with id %s\n' % u.id)
        connection.close()


