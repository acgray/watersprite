from django import forms
from django.forms.models import ModelForm
from judging.models import Rating

class RatingForm(ModelForm):
    overall = forms.IntegerField(initial=5)
    class Meta:
        model = Rating
        exclude = ['judge', 'entry']