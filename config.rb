# Require any additional compass plugins here.
require 'compass-blueprint'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "entries/static/css"
sass_dir = "entries/static/sass"
images_dir = "entries/static/images"
javascripts_dir = "entries/static/js"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

preferred_syntax = :sass
